TERRAFORM_MODULE_NAME="infra-registry"
TERRAFORM_MODULE_VERSION="0.0.3"
TERRAFORM_TARGET_PLATFORM="aws"
YOUR_TOKEN="PERSONAL-TOKEN-GIT"
GITLAB_API_V4_URL="https://gitlab.com/api/v4" # if you are using GitLab.com else use your own GitLab instance URL https://<my-dedicated-gitlab-instance>/api/v4
PROJECT_ID="PROJECT-ID-GIT"
GITLAB_URL="${GITLAB_API_V4_URL}/projects/${PROJECT_ID}/packages/terraform/modules/${TERRAFORM_MODULE_NAME}/${TERRAFORM_TARGET_PLATFORM}/${TERRAFORM_MODULE_VERSION}/file"

echo "Cleaning the TERRAFORM_MODULE_NAME. It should not contain spaces or underscores."
TERRAFORM_MODULE_NAME=$(echo "${TERRAFORM_MODULE_NAME}" | tr " _" -)

echo "Preparing the package."
tar -vczf ${TERRAFORM_MODULE_NAME}-${TERRAFORM_TARGET_PLATFORM}-${TERRAFORM_MODULE_VERSION}.tgz -C ./src --exclude=./.git .

echo "Publishing the package to ${GITLAB_URL}"

curl --location --header "PRIVATE-TOKEN: ${YOUR_TOKEN}" \
    --upload-file ${TERRAFORM_MODULE_NAME}-${TERRAFORM_TARGET_PLATFORM}-${TERRAFORM_MODULE_VERSION}.tgz \
    ${GITLAB_URL}

echo "Terraform module ${TERRAFORM_MODULE_NAME} version ${TERRAFORM_MODULE_VERSION} has been published to GitLab Infrastructure Registry"
