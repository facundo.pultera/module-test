bucket_name   = "my-test-bucket"
force_destroy = true
versioning    = "Enabled"
additional_tags = {
  "foo" = "bar"
}
context = {
  "cost_center" : "abc",
  "project_code" : "test",
  "region" : "us-east-1",
  "env_name" : "dev",
  "zone" : "ez",
  "tier" : "web"
}