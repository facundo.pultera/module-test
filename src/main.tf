variable "bucket_names" {
  type    = list(string)
  default = ["bucket1", "bucket2", "bucket3", "bucket4"]
}

# This is where you write your module code.
resource "random_string" "this" {
  length  = 10
  special = false
  upper   = false
}

locals {
  bucket_names_with_suffix = [
    for name in var.bucket_names != null ? var.bucket_names : [] :
    "s3-${var.context.zone}-${var.context.env_name}-${name}"
  ]
}

resource "aws_s3_bucket" "the_bucket" {
  count         = length(local.bucket_names_with_suffix)
  bucket        = local.bucket_names_with_suffix[count.index]
  force_destroy = var.force_destroy

  # checkov:skip=CKV_AWS_144: "Cross Region Replication is not mandatory"
  # checkov:skip=CKV_AWS_145: "KMS encryption is not mandatory for now."
  # checkov:skip=CKV_AWS_18: "S3 Access Logging is skipped for now."

  tags = merge(     
    {
      "Name" = local.bucket_names_with_suffix[count.index]
    },
    var.additional_tags,
    local.context_tags
  )
}

resource "aws_s3_bucket_server_side_encryption_configuration" "sse" {
  count  = length(local.bucket_names_with_suffix)
  bucket = aws_s3_bucket.the_bucket[count.index].id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "s3_bucket_versioning" {
  count  = length(local.bucket_names_with_suffix)
  bucket = aws_s3_bucket.the_bucket[count.index].id
  versioning_configuration {
    status = var.versioning
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  count  = length(local.bucket_names_with_suffix)
  bucket = aws_s3_bucket.the_bucket[count.index].id
  acl    = "private"
  depends_on = [aws_s3_bucket_ownership_controls.s3_bucket_acl_ownership]
}

# Resource to avoid error "AccessControlListNotSupported: The bucket does not allow ACLs"
resource "aws_s3_bucket_ownership_controls" "s3_bucket_acl_ownership" {
  count  = length(local.bucket_names_with_suffix)
  bucket = aws_s3_bucket.the_bucket[count.index].id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_public_access_block" "public_access_block" {
  count  = length(local.bucket_names_with_suffix)
  bucket = aws_s3_bucket.the_bucket[count.index].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
