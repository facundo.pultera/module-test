variable "bucket_name" {
  type        = string
  description = "The name of the bucket."

  # only alphanumeric characters and dashes are allowed
  validation {
    condition     = can(regex("^[a-z0-9-]+$", var.bucket_name))
    error_message = "The bucket name can contain only lowercase alphanumeric characters and dashes."
  }
}


variable "force_destroy" {
  type        = bool
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable. Defaults to false. Should only be true for testing purpose."
  default     = false
}

variable "versioning" {
  type        = string
  description = "The versioning state of the bucket. Valid values: Enabled, Suspended, or Disabled. Disabled should only be used when creating or importing resources that correspond to unversioned S3 buckets."
  default     = "Enabled"

  validation {
    condition     = can(regex("^(Enabled|Suspended|Disabled)$", var.versioning))
    error_message = "The versioning state of the bucket must be Enabled, Suspended, or Disabled."
  }
}

